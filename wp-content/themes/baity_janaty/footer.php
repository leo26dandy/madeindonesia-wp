<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BornDigital
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );
?>

		<footer id="footer" class="footer-1">
			<div class="main-footer widgets-dark typo-light">
				<div class="container">
					<div class="row footer-row">
					
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="widget subscribe no-box">
								<ul class="company-bio">
									<li>
										<h5 class="widget-title">Indonesia</h5>
									</li>
									<li class="company-address"></i>
										<a>Jalan Seturan Raya No.19, Dabag Sleman Yogyakarta</a> 
									</li>
									<li class="company-number">
										<a>+ 0274-624885</a>
									</li>
									<li class="company-email">
										<a>baity@baityjanaty.com</a>
									</li>
								</ul>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="widget no-box">
								<h5 class="widget-title">Home Search</h5>
								<ul class="thumbnail-widget">
									<li>
										<div class="thumb-content"><a href="#.">Project</a></div>	
									</li>
									<li>
										<div class="thumb-content"><a href="#.">Example Page</a></div>	
									</li>
									<li>
										<div class="thumb-content"><a href="#.">About</a></div>	
									</li>
									<li>
										<div class="thumb-content"><a href="#.">Privacy Policy</a></div>	
									</li>
								</ul>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-4">

							<div class="widget no-box">
								<img class="brand-logo" src="http://madeindonesia-wp.local/wp-content/uploads/2020/05/logo-1.png" alt="">

								<hr>
								<ul class="social-footer2">
									<h5 class="social-h5">
										Stay In Touch:
									</h5>
									<div class="col-md-12">
										<a href="https://twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a>
										<a href="instagram.com"><i class="fa fa-instagram" aria-hidden="true"></i></a>
										<a href="facebook.com"><i class="fa fa-facebook" aria-hidden="true"></i></a>
									</div>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>
				
		</footer>
	</div>
	<!-- /#page: the opening tag is in header.php -->

	<?php wp_footer(); ?>

	<script src="https://unpkg.com/swiper/js/swiper.js"></script>

	<!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper-container', {
      pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
      },
    });
  </script>

</body>
</html>
