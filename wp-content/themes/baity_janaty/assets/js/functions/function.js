import Swiper from 'swiper';

/**
 * Check if element is in dom
 * @param element @type string - element to check
 */
const isElementInDOM = element =>
    typeof element != undefined && element != null;
export let swiper = (element, options) => {
    const el = document.querySelector(element);
    if (isElementInDOM(el)) new Swiper(el, options);
};