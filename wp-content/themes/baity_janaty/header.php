<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BornDigital
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable = yes">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>

	<?php wp_head(); ?>
</head>

<body>
<div id="page-header" class="site-header">
	<!-- #page: the closing tag is in header.php -->
	<div id="content-wrapper">
		<header class="header header--bg">
				<div class="container">
				<nav class="navbar">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span> 
						</button>
						<a class="navbar-brand" href="<?php echo site_url(); ?>"></a>
					</div><!-- .site-branding -->
		
					<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="nav navbar-nav">
							<li class="about-li"><a href="<?php echo site_url(); ?>">About</a></li>
							<li><a href="#">Properties</a></li> 
							<li><a href="#">FWM</a></li> 
							<li class="contact-li"><a href="<?php echo site_url('/contact'); ?>">Contact</a></li>
						</ul>
					</div>
				</nav><!-- #site-navigation -->
				<?php if ( is_page( 34 ) ) {;?>
				<div class="header__content text-center">
					<span class="header__content__block">View The Extraordinary</span>
					<h1 class="header__content__title">Let Us Guide You Home</h1>
					<a class="header__button" href="#">See more</a>
				</div><!-- .header__content -->
				<?php } ;?>
			</div><!-- .container -->
		</header><!-- #masthead -->
		<div id="content" class="site-content">
