<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BornDigital
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );

get_header(); ?>

    <a id="popuplink" href="#inline" style="display:none;"></a>
    <div id="inline" style="display:none; border-style:solid; border-color:blue; text-align:center;">
        <a href="javascript:;" onclick="jQuery.fancybox.close();" style="font-family:serif; color: black; padding-left: 350px; text-decoration:none; font-weight: bold;">X</a>    
        <img src="#" alt="" id="logo">
        <h1 style="font-weight:bold; font-family:serif; margin-top:0px;">Attention Please</h1>
        <h5 style="font-size: 20px">We have special offer for customers who have paid 50% in installments on credit.</h5>
    </div>

	<section class="about">
        <div class="container about__container--narrow">
            <div class="page-section-about">
                <div class="row gutters-80">
                    <div class="col-md-4 image__about">
                        <div class="profile__image">
                            <img src="http://madeindonesia-wp.local/wp-content/uploads/2020/05/peopke.jpg" alt="Profile Photo">
                        </div>
                    </div> <!-- .image__about -->

                    <div class="col-md-8 about__content">
                        <div class="row row--margin-top">
                            <div class="bio__profile">
                                <h3 class="about__bio__title">'Sup Internet!</h3>
                                <h1 class="about__bio__name">I am Tim Ferris</h1>
                                <p class="about__bio__par">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vulputate libero sed nisi interdum, nec varius nulla dictum. Maecenas ac varius metus. Morbi id lectus elit.</p>
                            </div>
                            <div class="profile__link">
                                <a class="profile__button" href="#">Learn More</a>
                            </div>
                        </div>
                    </div> <!-- .about__content -->

                </div>
            </div>
        </div>
    </section> <!-- .about -->

    <section id="portfolio">
        <div class="container wow fadeInUp">
            <div class="row">
                <div class="col-md-12">
                    <p class="section-description">Find Your Nook</p>
                    <h3 class="section-title">What Lifestyle Do You Want To Live?</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <a class="portfolio-item" style="background-image: url('http://madeindonesia-wp.local/wp-content/uploads/2020/05/Cozy_place.jpg');" href="#">
                        <div class="details">
                            <h4>Cozy</h4>
                        </div>
                    </a>
                </div>

                <div class="col-md-4">
                    <a class="portfolio-item" style="background-image: url('http://madeindonesia-wp.local/wp-content/uploads/2020/05/rumah.jpg');" href="#">
                        <div class="details">
                            <h4>Ex-Burbs</h4>
                        </div>
                    </a>
                </div>

                <div class="col-md-4">
                    <a class="portfolio-item" style="background-image: url('http://madeindonesia-wp.local/wp-content/uploads/2020/05/photo-1516156008625-3a9d6067fab5.jpg');" href="#">
                        <div class="details">
                            <h4>Neighbors</h4>
                        </div>
                    </a>
                </div>

                <div class="col-md-4">
                    <a class="portfolio-item" style="background-image: url('http://madeindonesia-wp.local/wp-content/uploads/2020/05/urban@2x.jpg');" href="#">
                        <div class="details">
                            <h4>Fair Value</h4>
                        </div>
                    </a>
                </div>

                <div class="col-md-4">
                    <a class="portfolio-item" style="background-image: url('http://madeindonesia-wp.local/wp-content/uploads/2020/05/photo-1524293568345-75d62c3664f7.jpg');" href="#">
                        <div class="details">
                            <h4>Modern</h4>
                        </div>
                    </a>
                </div>

                <div class="col-md-4">
                    <a class="portfolio-item" style="background-image: url('http://madeindonesia-wp.local/wp-content/uploads/2020/05/photo-1475855581690-80accde3ae2b.jpg');" href="#">
                        <div class="details">
                            <h4>Family Friendly</h4>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </section> <!-- .portofolio -->

    <section id="testimonial" data-stellar-background-ratio="0.5">
          <div class="swiper-container">
              <div class="col-md-12 col-sm-12 title-testimonial">
                  <div class="section-title-testimonial">
                      <h3>Testimonials</h3>
                    </div>
                    <div class="section-title-testimonial">
                        <h1>Our Customer Stories</h1>
                    </div>
                </div>  
                
                <div class="row swiper-wrapper">
                     <div class="swiper-slide">
                        <div class="col-md-12 col-sm-12 bio-testimonial">
                             <div class="col-md-6 col-sm-6">
                                  <div class="item-img">
                                      <img src="http://madeindonesia-wp.local/wp-content/uploads/2020/05/Ini-Budi-scaled.jpg" alt="photos">
                                  </div>
                             </div>
                             <div class="col-md-6 col-sm-6">
                                  <div class="item-bio">
                                       <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum ullamcorper nulla non.</p>
                                        <div class="tst-author">
                                            <h2>Digital Carlson</h2>
                                        </div>
                                  </div>
                             </div>
                        </div>
                        ...
                    </div>
               </div>
          </div>
     </section> <!-- .testimonial -->
    </div>
<?php
get_sidebar();
get_footer();
