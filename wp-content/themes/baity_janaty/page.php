<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BornDigital
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );

get_header();

while ( have_posts() ) {
    the_post(); ?>

	<div class="main-page">

		<h1 class="title-page" style="font-family: serif"> <?php the_title() ;?> </h1>
		<?php the_content() ;?>

	</div>

<?php
get_footer();
}
?>