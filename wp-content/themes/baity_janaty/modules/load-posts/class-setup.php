<?php
/**
 * Example of component
 *
 * @package BornDigital
 */

namespace BD\LoadPosts;

/**
 * Class to setup the component
 */
class Setup
{
	/**
	 * Current module directory
	 *
	 * @var string
	 */
	private $dir;

	/**
	 * Current module url
	 *
	 * @var string
	 */
	private $url;

	/**
	 * Setup the flow
	 */
	public function __construct()
	{
		$this->dir = MODULES_DIR . '/load-posts';
        $this->url = MODULES_URL . '/load-posts';
        
        //alll the hooks and filters

        add_action( 'wp_enqueue_scripts', [ $this, 'loadsetup' ] );
    }

   public function loadsetup()
   {

    global $post;

    wp_enqueue_script(
        'main-js',
        get_template_directory_uri() . '/modules/load-posts/assets/js/load-posts.js',
        array( 'jquery' ),
        'auto',
        true
    );
    wp_localize_script(
        'main-js', 'main_js_get_id', array( 'postID' => $post->ID)
    );

    wp_enqueue_script(
        'bootstrap-js',
        get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js',
        array( 'jquery' ),
        'auto',
        true
    );

    wp_register_script( 'fancybox-min-js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js' );
    wp_enqueue_script( 'fancybox-min-js' );
    
    wp_register_script( 'jquery-cookie-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.0/jquery.cookie.js' );
    wp_enqueue_script( 'jquery-cookie-js' );
    
    
    // load css
    wp_register_style( 'swiper-min-css', 'https://unpkg.com/swiper/css/swiper.min.css' );
    wp_enqueue_style( 'swiper-min-css' );
    
    wp_register_style( 'swiper-css', 'https://unpkg.com/swiper/css/swiper.css' );
    wp_enqueue_style( 'swiper-css' );
    
    wp_register_style( 'fancybox-min-css', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css' );
    wp_enqueue_style( 'fancybox-min-css' );

    wp_register_style( 'fontawesome-min-css', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
    wp_enqueue_style( 'fontawesome-min-css' );

    wp_enqueue_style(
        'bootstrap-css',
        get_stylesheet_directory_uri() . '/assets/bootstrap/css/bootstrap.css',
		'auto',
        true
    );
    
    wp_enqueue_style(
        'custom-css',
        get_stylesheet_directory_uri() . '/modules/load-posts/assets/css/custom.css',
		'auto',
		true
    );
   }

}

new Setup();
