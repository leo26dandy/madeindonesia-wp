<?php
/**
 * Konfigurasi dasar WordPress.
 *
 * Berkas ini berisi konfigurasi-konfigurasi berikut: Pengaturan MySQL, Awalan Tabel,
 * Kunci Rahasia, Bahasa WordPress, dan ABSPATH. Anda dapat menemukan informasi lebih
 * lanjut dengan mengunjungi Halaman Codex {@link http://codex.wordpress.org/Editing_wp-config.php
 * Menyunting wp-config.php}. Anda dapat memperoleh pengaturan MySQL dari web host Anda.
 *
 * Berkas ini digunakan oleh skrip penciptaan wp-config.php selama proses instalasi.
 * Anda tidak perlu menggunakan situs web, Anda dapat langsung menyalin berkas ini ke
 * "wp-config.php" dan mengisi nilai-nilainya.
 *
 * @package WordPress
 */

// ** Pengaturan MySQL - Anda dapat memperoleh informasi ini dari web host Anda ** //
/** Nama basis data untuk WordPress */
define( 'DB_NAME', 'madeindonesia' );

/** Nama pengguna basis data MySQL */
define( 'DB_USER', 'root' );

/** Kata sandi basis data MySQL */
define( 'DB_PASSWORD', '' );

/** Nama host MySQL */
define( 'DB_HOST', 'localhost' );

/** Set Karakter Basis Data yang digunakan untuk menciptakan tabel basis data. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Jenis Collate Basis Data. Jangan ubah ini jika ragu. */
define('DB_COLLATE', '');

/**#@+
 * Kunci Otentifikasi Unik dan Garam.
 *
 * Ubah baris berikut menjadi frase unik!
 * Anda dapat menciptakan frase-frase ini menggunakan {@link https://api.wordpress.org/secret-key/1.1/salt/ Layanan kunci-rahasia WordPress.org}
 * Anda dapat mengubah baris-baris berikut kapanpun untuk mencabut validasi seluruh cookies. Hal ini akan memaksa seluruh pengguna untuk masuk log ulang.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '*XF9W7(S?Z8#n!Lm=e5vo.&%,XX=fU:+jAjN@)Xn^rbiM wE3|l#FSkT`n:2r >E' );
define( 'SECURE_AUTH_KEY',  'wKcHu lbmCmPxZ3Pd#=Le7Ttl,7}6;^OD X},*$S~TQg,R$!Y9q(6qh^Bk|m}EPi' );
define( 'LOGGED_IN_KEY',    '>l% O36/eR6a66LJ6eAHZJ5beqP$;Z6zX`KS8FD*L$S*7GVUgc?):fNUoL]B:C(h' );
define( 'NONCE_KEY',        '|#ki-kg0VC1J[GBUdXo=9~G#Ai^gt9V|?9BDDdYLij5CzNXr,RA!dWvX]gH=Yla:' );
define( 'AUTH_SALT',        'e0@P0rZ4x3l@TQ46OEKAdQk}*Ix~> MOYvg[&7TTmK1)~WP~tK?eUyo|(om3{~_}' );
define( 'SECURE_AUTH_SALT', '8C!N(xfzm=T$R5~XJa>3ZW,s2I4R:Ze3?:,kv?6MLlu/7$}Pk=L-SXg<t%9#vp(:' );
define( 'LOGGED_IN_SALT',   'x=3}O;Hp=a0s4H2C%IUB yQSe$jSMO[bo}RG~CqJUZtQ-l/UsG)s)0VQqmwqdm55' );
define( 'NONCE_SALT',       'l_I<x-MZ_US4S;` JLjb-R1.EpC]_#GH[qMy|[_Di?9> n18s[13PclObO$mm#5&' );

/**#@-*/

/**
 * Awalan Tabel Basis Data WordPress.
 *
 * Anda dapat memiliki beberapa instalasi di dalam satu basis data jika Anda memberikan awalan unik
 * kepada masing-masing tabel. Harap hanya masukkan angka, huruf, dan garis bawah!
 */
$table_prefix = 'wp_';

/**
 * Untuk pengembang: Moda pengawakutuan WordPress.
 *
 * Ubah ini menjadi "true" untuk mengaktifkan tampilan peringatan selama pengembangan.
 * Sangat disarankan agar pengembang plugin dan tema menggunakan WP_DEBUG
 * di lingkungan pengembangan mereka.
 */
define('WP_DEBUG', true);

/* Cukup, berhenti menyunting! Selamat ngeblog. */

/** Lokasi absolut direktori WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Menentukan variabel-variabel WordPress berkas-berkas yang disertakan. */
require_once(ABSPATH . 'wp-settings.php');
